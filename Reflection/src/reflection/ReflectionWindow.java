package reflection;

import java.lang.reflect.*;
import java.beans.*;

import java.awt.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.table.*;

public class ReflectionWindow extends JFrame {

    public ReflectionWindow() {
        initComponents();
        displayComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jSplitPane1 = new javax.swing.JSplitPane();
        jSplitPane2 = new javax.swing.JSplitPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        propTable = new javax.swing.JTable();
        designer = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tree = new javax.swing.JTree();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jToolBar1 = new javax.swing.JToolBar();
        jToolBar2 = new javax.swing.JToolBar();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jSplitPane1.setDividerLocation(300);

        jSplitPane2.setDividerLocation(200);

        propTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String []
            {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(propTable);

        jSplitPane2.setRightComponent(jScrollPane2);

        javax.swing.GroupLayout designerLayout = new javax.swing.GroupLayout(designer);
        designer.setLayout(designerLayout);
        designerLayout.setHorizontalGroup(
            designerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 199, Short.MAX_VALUE)
        );
        designerLayout.setVerticalGroup(
            designerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 464, Short.MAX_VALUE)
        );

        jSplitPane2.setLeftComponent(designer);

        jSplitPane1.setRightComponent(jSplitPane2);

        tree.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener()
        {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt)
            {
                treeValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(tree);

        jSplitPane1.setLeftComponent(jScrollPane1);

        jToolBar1.setRollover(true);
        jTabbedPane1.addTab("tab1", jToolBar1);

        jToolBar2.setRollover(true);
        jTabbedPane1.addTab("tab2", jToolBar2);

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 857, Short.MAX_VALUE)
            .addComponent(jSplitPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSplitPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 468, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addComponents (DefaultMutableTreeNode target, Container container)
    {
        for (Component c : container.getComponents())
        {
           DefaultMutableTreeNode node = new DefaultMutableTreeNode (c);
           if (c instanceof Container)
           {
              addComponents (node, (Container) c);
           }
           target.add (node);
        }
    }
           
    private void displayComponents ()
    {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode (this);
        addComponents (root, this);
        DefaultTreeModel model = new DefaultTreeModel (root);
        tree.setModel (model);
    }
    
    private void treeValueChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_treeValueChanged
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
        if (node != null)
        {
            displayProperties (node.getUserObject());
        }
    }//GEN-LAST:event_treeValueChanged

    Object propObject;
    
    private void displayProperties (Object obj)
    {
        try 
        {
            DefaultTableModel model = new DefaultTableModel ();
            model.addColumn ("name");
            model.addColumn ("value");
        
            propObject = obj; // remember, needed in propTableChanged
            Class cls = obj.getClass ();
            BeanInfo info = Introspector.getBeanInfo (cls);
            PropertyDescriptor [] properties = info.getPropertyDescriptors ();
            for (PropertyDescriptor p : properties)
            {
                String name = p.getName ();
                String value = "";
                try 
                {
                    
                    Method read = p.getReadMethod();
                    if (read != null)
                       value = "" + read.invoke (obj);
                } 
                catch (IllegalAccessException ex) { } 
                catch (IllegalArgumentException ex) { } 
                catch (InvocationTargetException ex) { }
                Object [] line = {name, value};
                model.addRow (line);
            }
            propTable.setModel (model);
            TableModelListener listener = new TableModelListener() 
            {
                @Override
                public void tableChanged (TableModelEvent evt) 
                {
                   propTableChanged (evt);        
                }
            };
            model.addTableModelListener(listener);
        } 
        catch (IntrospectionException ex) 
        {
        }
    }    
    
    private void propTableChanged (TableModelEvent evt)
    {
        int line = evt.getFirstRow ();
        int col = evt.getColumn ();
        
        if (col == 1)
        {
            TableModel model = (TableModel) evt.getSource();
            String name = (String) model.getValueAt (line, 0);
            String value = (String) model.getValueAt (line, 1);

            Object obj = propObject;
            Class cls = obj.getClass ();
            BeanInfo info;
            try 
            {
                info = Introspector.getBeanInfo (cls);
                for (PropertyDescriptor prop : info.getPropertyDescriptors ())
                if (prop.getName () == name)
                {
                    Method write = prop.getWriteMethod ();
                    if (write != null)
                    {
                        Object object_value = value; // String
                        Class type = prop.getPropertyType ();
                        if (type == boolean.class)
                        {
                            if (value.equals ("true"))
                                object_value = true;
                            if (value.equals ("false"))
                                object_value = false;
                        }
                        else if (type == int.class)
                        {
                            try
                            {
                               value = value.trim ();
                               int n = Integer.valueOf (value);
                               object_value = n;
                            }
                            catch (NumberFormatException ex) { System.out.println (ex); }
                        }
                        
                        try 
                        {
                            System.out.println ("SET " + name + " = " + object_value + " : " + object_value.getClass ());
                            write.invoke (obj, new Object [] { object_value });
                        } 
                        catch (IllegalAccessException ex) { System.out.println (ex); } 
                        catch (IllegalArgumentException ex) { System.out.println (ex); }
                        catch (InvocationTargetException ex) { System.out.println (ex);}
                    }
                }
            } 
            catch (IntrospectionException ex) {  }
        }
    }
   
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ReflectionWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel designer;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JTable propTable;
    private javax.swing.JTree tree;
    // End of variables declaration//GEN-END:variables
}
