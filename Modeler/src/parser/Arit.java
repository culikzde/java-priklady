package parser;

public class Arit {
    public String name;
    public Arit left;
    public Arit middle;
    public Arit right;
    public Arit (String name0) { name = name0; }
    public String toString () { return name; }
}
