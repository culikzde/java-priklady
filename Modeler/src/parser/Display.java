package parser;

import java.lang.reflect.*;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.table.*;

public class Display {
    
    public JTree tree;
    public JTable table;
    public JTextArea input;
    public JTextArea output;

    private DefaultTreeModel m; 
    private DefaultTableModel tm;
    
    private void run ()
    {
        Parser p = new Parser (input.getText ());
        output.setText ("");
        try
        {
            Arit a = p.statement();
            displayTree (a);
            output.append("O.K.");
        }
        catch (RuntimeException ex)
        {
            output.append(ex.toString ());
        }
    }
    
    private void displayTree (Arit a)
    {
       DefaultMutableTreeNode root = displayBranch (a) ;
       m = new DefaultTreeModel (root);
       tree.setModel (m);
    }

    private DefaultMutableTreeNode displayBranch (Arit a)
    {   
       DefaultMutableTreeNode node = new DefaultMutableTreeNode (a);
       if (a.left != null)
             node.add (displayBranch (a.left)) ;
       if (a.middle != null)
             node.add (displayBranch (a.middle)) ;
       if (a.right != null)
             node.add (displayBranch (a.right)) ;
       return node;
    } 
    
    private void displayTable (Object obj)
    {
        tm = new DefaultTableModel ();
        tm.addColumn ("name");
        tm.addColumn ("value");
        
        Class cls = obj.getClass();
        for (Field field : cls.getFields())
        {
            String name = field.getName ();
            String value = "";
            
            try 
            {
                Object answer = field.get (obj);
                if (answer != null)
                    value = answer.toString();
            }
            catch (IllegalArgumentException ex) { }
            catch (IllegalAccessException ex) { }
    
            tm.addRow (new Object [] {name, value});
        }
        
        table.setModel (tm);
    }
    
    private void treeValueChanged (javax.swing.event.TreeSelectionEvent evt) {
        Object obj  = tree.getLastSelectedPathComponent();
        if (obj instanceof DefaultMutableTreeNode)
        {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) obj;
            displayTable (node.getUserObject());
        }
    }
}
