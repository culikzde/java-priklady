package parser;

public class Parser extends Lexer {
    
    public Parser (String input)
    {
        super (input);
    }
    
    public Arit factor ()
    {
        Arit result = null;
        if (isSeparator("("))
        {
           nextToken (); // preskocit (
           result = expr ();
           checkSeparator (")");
        }
        else if (kind == ident || kind == number)
        {
           result = new Arit (token);
           nextToken ();
        }
        else
            error ("Identifier or number or sub-expression expected");
        return result;
    }
    
    public Arit term ()
    {
        Arit result = factor ();
        while (isSeparator ("*") || isSeparator ("/"))
        {
            Arit temp = result;
            result = new Arit (token);
            nextToken();
            result.left = temp;
            result.right = factor ();
        }
        return result;
    }
    
    public Arit simpleExpr ()
    {
        Arit result = term ();
        while (isSeparator ("+") || isSeparator ("-"))
        {
            Arit temp = result;
            result = new Arit (token);
            nextToken();
            result.left = temp;
            result.right = term ();
        }
        return result;
    }
    
    public Arit expr ()
    {
        Arit result = simpleExpr ();
        if (isSeparator ("="))
        {
            Arit temp = result;
            result = new Arit (token);
            nextToken();
            result.left = temp;
            result.right = expr ();
        }
        return result;
    }
    
    public Arit ifStatement ()
    {
        checkKeyword("if");
        Arit result = new Arit ("if");
        checkSeparator ("(");
        result.left = expr (); // condition
        checkSeparator (")");
        result.middle = statement (); // then statement
        if (isKeyword ("else"))
        {
            nextToken (); // preskocit else
            result.right = statement (); // else statement
        }
        return result;
    }

    public Arit whileStatement ()
    {
        checkKeyword("while");
        Arit result = new Arit ("while");
        checkSeparator ("(");
        result.left = expr (); // condition
        checkSeparator (")");
        result.middle = statement (); // then statement
        return result;
    }

    public Arit statement ()
    {
        Arit result = null;
        if (isKeyword ("if"))
            result = ifStatement();
        else if (isKeyword ("while"))
            result = whileStatement();
        else 
        {
            result = expr ();
            checkSeparator(";");
        }
        return result;
    }
}
