package experiment;

import java.lang.*;

public class Experiment 
{
    private boolean b;
    private char c = 'Č';
    private short i;
    private int j;
    private long k;
    private float x;
    private double y;
    
    public static int cnt;
    
    public Experiment ()
    {
        System.out.println ("Vytvarim novou instanci tridy Experiment");
        java.io.PrintStream out = System.out; 
        
        i ++; // zvetsime nestatickou promennou, v kazde instanci je samostatna promenna i
        
        out.print ("b="); out.println (b);
        out.print ("c="); out.print (c); out.print (", vnitrni hodnota c="); out.println ((int) c);
        out.print ("i="); out.println (i);
        out.print ("j="); out.println (j);
        out.print ("k="); out.println (k);
        out.print ("x="); out.println (x);
        out.print ("y="); out.println (y);
        
        cnt ++; // zvetsime statickou promennou, promenna cnt je jen jedna v celem programu
        
        out.print ("cnt="); out.println (cnt);
        out.println ();
    }
    
    public static void main(String[] args) 
    {
         // Experiment.cnt = 10;
         Experiment e = new Experiment ();
         Experiment t = new Experiment ();
    }   
}
