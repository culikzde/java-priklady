package tree;

import java.io.*;
import javax.swing.*;
import javax.swing.tree.*;

public class TreeWindow extends javax.swing.JFrame {

    DefaultMutableTreeNode root;
    DefaultTreeModel model;
    
    public TreeWindow() {
        initComponents();

        /*
        root = new DefaultMutableTreeNode ("first");        
        DefaultMutableTreeNode node = new DefaultMutableTreeNode ("second");
        root.add (node);
        */
        
        File f = new File ("..");
        try 
        {
            f = f.getCanonicalFile (); // prevedeme .. na opravdove jmeno adresare
            
        } 
        catch (IOException ex) { }
        root = showFile (f);

        model = new DefaultTreeModel (root);
        jTree1.setModel (model);
    }

    private DefaultMutableTreeNode showFile (File f)
    {
        DefaultMutableTreeNode node = new DefaultMutableTreeNode (f.getName());

        if (f.isDirectory())
        {
        
           File [] items = f.listFiles();
           for (File t : items)
           {
               node.add (showFile (t));
           }
        }
        
        return node;
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();
        mainMenu = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        quitMenuItem = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        addMenuItem = new javax.swing.JMenuItem();
        editMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTree1.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
                jTree1ValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jTree1);

        fileMenu.setMnemonic('F');
        fileMenu.setText("File");

        quitMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        quitMenuItem.setMnemonic('Q');
        quitMenuItem.setText("Quit");
        quitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitClick(evt);
            }
        });
        fileMenu.add(quitMenuItem);

        mainMenu.add(fileMenu);

        editMenu.setMnemonic('E');
        editMenu.setText("Edit");

        addMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F7, 0));
        addMenuItem.setMnemonic('A');
        addMenuItem.setText("Add Tree Item");
        addMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addClick(evt);
            }
        });
        editMenu.add(addMenuItem);

        editMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F8, 0));
        editMenuItem.setMnemonic('E');
        editMenuItem.setText("Edit Tree Item");
        editMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editClick(evt);
            }
        });
        editMenu.add(editMenuItem);

        mainMenu.add(editMenu);

        setJMenuBar(mainMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 752, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 502, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private DefaultMutableTreeNode getNode ()
    {
        return (DefaultMutableTreeNode) jTree1.getLastSelectedPathComponent ();
    }
    
    private void jTree1ValueChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_jTree1ValueChanged
       DefaultMutableTreeNode node = getNode ();
       if (node != null)
       {
           setTitle (node.toString());
       }
    }//GEN-LAST:event_jTree1ValueChanged

    private void quitClick(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitClick
        dispose ();
    }//GEN-LAST:event_quitClick
  
    private void addClick(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addClick
        DefaultMutableTreeNode node = getNode ();
        if (node != null)
        {
            DefaultMutableTreeNode item = new DefaultMutableTreeNode ("new item");
            node.add (item);
          
            model.nodesWereInserted (node, new int [] {node.getIndex (item)});
        }
    }//GEN-LAST:event_addClick

    private void editClick(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editClick
        // TODO add your handling code here:
        DefaultMutableTreeNode node = getNode ();
        if (node != null)
        {
            String name = JOptionPane.showInputDialog (this, "enter new name");
            node.setUserObject (name);
            model.nodeChanged (node);
        }
        
    }//GEN-LAST:event_editClick

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TreeWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TreeWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TreeWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TreeWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TreeWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem addMenuItem;
    private javax.swing.JMenu editMenu;
    private javax.swing.JMenuItem editMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTree jTree1;
    private javax.swing.JMenuBar mainMenu;
    private javax.swing.JMenuItem quitMenuItem;
    // End of variables declaration//GEN-END:variables
}
