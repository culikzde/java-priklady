package filetree;

import java.awt.image.BufferedImage;
import java.io.*;
import java.text.*;
import java.time.format.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.tree.*;

public class FileWindow extends javax.swing.JFrame {

    private DefaultTreeModel treeModel;
    private DefaultTableModel tableModel;
    private Vector <String> fileInfo = new Vector <String> ();
    
    public FileWindow() {
        initComponents();
        
        // tableModel = new DefaultTableModel ();
        // table.setModel (tableModel);

        tableModel = (DefaultTableModel) table.getModel ();

        /*
        table.getSelectionModel().addListSelectionListener
        ( 
           new ListSelectionListener () 
           {
               public void valueChanged (ListSelectionEvent evt) 
               {
                   tableSelectionChanged (evt);
               }
           }
        );
        */
       
        showDirectories ();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        tree = new javax.swing.JTree();
        jSplitPane2 = new javax.swing.JSplitPane();
        tabs = new javax.swing.JTabbedPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("File Tree");

        jSplitPane1.setDividerLocation(200);

        tree.addTreeExpansionListener(new javax.swing.event.TreeExpansionListener()
        {
            public void treeExpanded(javax.swing.event.TreeExpansionEvent evt)
            {
                treeTreeExpanded(evt);
            }
            public void treeCollapsed(javax.swing.event.TreeExpansionEvent evt)
            {
            }
        });
        tree.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener()
        {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt)
            {
                treeValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(tree);

        jSplitPane1.setLeftComponent(jScrollPane1);

        jSplitPane2.setDividerLocation(200);
        jSplitPane2.setRightComponent(tabs);

        table.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String []
            {
                "Name", "Size", "Time"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean []
            {
                false, false, false
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        table.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                tableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(table);

        jSplitPane2.setLeftComponent(jScrollPane2);

        jSplitPane1.setRightComponent(jSplitPane2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 924, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 587, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void showDirectories ()
    {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode ("Files");
        treeModel = new DefaultTreeModel (root);
        tree.setModel (treeModel);

        // import java.io.*;
        // File dir = new File ("C:\\");
        // File [] list = dir.listFiles ();
        
        File [] list = File.listRoots ();
        int cnt = 0;
        for (File f : list)
        {
            if (f.isDirectory ())
            {
                MyData data = new MyData ();
                data.name = f.getAbsolutePath ();
                data.path = f.getAbsolutePath();

                DefaultMutableTreeNode node = new DefaultMutableTreeNode (data);

                treeModel.insertNodeInto (node, root, cnt);
                cnt ++;
            }
        }
        
        tree.expandRow (0);
    }
    
    private void treeValueChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_treeValueChanged
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
        if (node != null)
        {
            Object obj = node.getUserObject ();
            if (obj instanceof MyData)
            {
                MyData data = (MyData) node.getUserObject ();
                // System.out.println ("SELECT " + data.path);
                addFiles (data.path);
            }
        }
    }//GEN-LAST:event_treeValueChanged

    private void treeTreeExpanded(javax.swing.event.TreeExpansionEvent evt)//GEN-FIRST:event_treeTreeExpanded
    {//GEN-HEADEREND:event_treeTreeExpanded
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) evt.getPath ().getLastPathComponent ();
        if (node != null)
        {
            Object obj = node.getUserObject ();
            if (obj instanceof MyData)
            {
                MyData data = (MyData) node.getUserObject ();
                // System.out.println ("EXPANDED " + data.path);
                
                if (! data.ready)
                {
                    addDirectory (node, data);
                }
            }
                
            int cnt = node.getChildCount ();
            for (int i = 0 ; i < cnt; i ++)
            {
                DefaultMutableTreeNode n = (DefaultMutableTreeNode) node.getChildAt (i);
                MyData d = (MyData) n.getUserObject ();
                if (! d.ready)
                {
                    // System.out.println ("ADD " + d.path);
                    addDirectory (n, d);
                }
            }
        }
    }//GEN-LAST:event_treeTreeExpanded

    private void addDirectory (DefaultMutableTreeNode topNode, MyData topData)
    {
        topData.ready = true;

        File dir = new File (topData.path);
        File [] list = dir.listFiles ();

        if (list != null)
        {
            Arrays.sort (list);
            int cnt = 0;
            for (File f : list)
            {
                if (f.isDirectory ())
                {
                    MyData data = new MyData ();
                    data.name = f.getName ();
                    data.path = f.getAbsolutePath();

                    DefaultMutableTreeNode node = new DefaultMutableTreeNode (data);

                    treeModel.insertNodeInto(node, topNode, cnt);
                    cnt ++;
                }
            }
        }
    }
    
    private String formatNumber (long number)
    {
        String s = "" + number;
        int len = s.length ();
        String result = "";
        int cnt = 0;
        for (int i = len-1; i >= 0; i--)
        {
            result = s.charAt (i) + result;
            cnt ++;
            if (cnt % 4 == 3)
            {
                result = ' ' + result;
                cnt ++;
            }
        }
        
        while (cnt <= 12)
        {
            result = ' ' + result;
            cnt ++;
        }
                
        return result;
    }
    
    private void addFiles (String path)
    {
        tableModel.setRowCount (0);
        fileInfo.clear ();
        
        File dir = new File (path);
        File [] list = dir.listFiles ();
    
        SimpleDateFormat fmt = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
        
        if (list != null)
            for (File f : list)
            {
                if (f.isFile ())
                {
                    fileInfo.add (f.getAbsolutePath());
                    
                    String name = f.getName ();
                    long size = f.length ();
                    Date date = new Date (f.lastModified());
                    
                    Object [] line = { name, formatNumber (size), fmt.format (date) };
                    tableModel.addRow (line);
                }
           }
    }


    private void tableMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tableMouseClicked
    {//GEN-HEADEREND:event_tableMouseClicked
        if (evt.getClickCount () == 2)
        {
            int line = table.getSelectedRow ();
            if (line >= 0)
            {
                String path = fileInfo.elementAt (line);
                System.out.println ("DOUBLE CLICK " + path);
 
                if (path.endsWith (".java"))
                {
                    showTextFile (path);
                }
                if (path.endsWith (".png") || path.endsWith (".bmp") || path.endsWith (".gif") || path.endsWith (".jpeg"))
                {
                    showImage (path);
                }
                if (path.endsWith (".csv"))
                {
                    showTable (path);
                }
            }
        }
    }//GEN-LAST:event_tableMouseClicked

    /*
    private void tableSelectionChanged (ListSelectionEvent evt) 
    {
        int line = evt.getFirstIndex ();
    }
    */
    
    private String fileName (String path)
    {
        File f = new File (path);
        return f.getName ();
    }
    
    private void showTextFile (String path)
    {
        try 
        {
            StringBuilder t = new StringBuilder ();
            FileReader r = new FileReader (path);
            BufferedReader b = new BufferedReader (r);
            while (b.ready ())
            {
                t.append (b.readLine() + "\n");
            }

            JTextArea edit = new JTextArea ();
            edit.setText (t.toString());
            tabs.addTab (fileName (path), edit);
            tabs.setSelectedComponent (edit);
        } 
        catch (FileNotFoundException ex) { } 
        catch (IOException ex) { }
    }
    
    private void showImage (String path)
    {
        try
        {
            BufferedImage img = ImageIO.read (new File (path));
            Icon icon = new ImageIcon (img);
            
            JLabel label = new JLabel ();
            label.setIcon (icon);
            tabs.addTab (fileName (path), label);
            tabs.setSelectedComponent (label);
        } 
        catch (IOException ex) { }
    }

    private void showTable (String path)
    {
        try
        {
            FileReader r = new FileReader (path);
            BufferedReader b = new BufferedReader (r);
            
            Vector <String []> lines = new Vector <String []> ();
            int columns = 0;
            while (b.ready ())
            {
                String s = b.readLine ();
                String [] data = s.split (",");
                int cnt = data.length;
                if (cnt > columns) columns = cnt;
                lines.add (data);
            }

            JTable t = new JTable ();
            t.setShowGrid (true);
            DefaultTableModel m = (DefaultTableModel) t.getModel ();
            
            
            m.setColumnCount (columns);
            for (String [ ] line : lines)
                m.addRow (line);
            
            tabs.addTab (fileName (path), t);
            tabs.setSelectedComponent (t);
        } 
        catch (IOException ex) { }
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FileWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FileWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FileWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FileWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FileWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JTable table;
    private javax.swing.JTabbedPane tabs;
    private javax.swing.JTree tree;
    // End of variables declaration//GEN-END:variables
}
