package experiment;

import java.awt.*;
import javax.swing.table.*;

public class Display extends javax.swing.JFrame {

    public Display() {
        initComponents();
    }

    public Display (String name, int [] a) {
        this ();
        put (name, a);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.setAlignmentX(0.7F);
        jTable1.setRowHeight(32);
        jScrollPane1.setViewportView(jTable1);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, 760, 59));

        jLabel1.setText("jLabel1");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 12, -1, -1));

        jTextField1.setText("jTextField1");
        getContentPane().add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 30, 70, 30));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void put (String name, int [] a )
    {
        setTitle (name);
        if (a == null)
        {
           jLabel1.setText ("null");
           jLabel1.setForeground (new Color (255, 0, 0));
           jLabel1.setFont(new java.awt.Font("Dialog", 0, 36)); 
           
           jTextField1.setText ("null");
           jTextField1.setForeground (new Color (255, 0, 0));
           // remove (jTable1);
           // getContentPane().remove (jS)
           jTextField1.setVisible (false);        
           jScrollPane1.setVisible (false);        
        }
        else
        {
            int n = a.length;

            jLabel1.setText (name + ".length");
            jTextField1.setText ("" + n);// convert to string

            String [] title = new String [n];
            String [] [] data = new String [1] [n];

            for (int i = 0; i < n; i++) 
            {
                title [i] = name + "[" + i + "]" ;
                data [0][i] = "" + a[i]; // convert to string

            }

            DefaultTableModel model = new DefaultTableModel (data, title);
            jTable1.setModel (model);
            
            if (n== 0) 
               jScrollPane1.setVisible (false); 
        }
    }
    
    private static int x = 10;
    private static int y = 10;
    
    public static void demo (String name, int a [])
    {
        Display w = new Display (name, a);
        w.setVisible (true);
        w.setLocation(x, y);
        y = y + w.getHeight() + 10;
    }
    
    public static void main(String args[]) 
    {
        int a [] = { 1, 2, 3 ,4, 5, 6, 7 };
        int b [] = new int [10];
        int c [] = { };
        int e [] = null;
        
        demo ("a", a);
        demo ("b", b);
        demo ("c", c);
        demo ("e", e);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
