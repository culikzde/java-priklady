package experiment;

import java.awt.*;
import org.netbeans.lib.awtextra.AbsoluteConstraints;

public class Display2D extends javax.swing.JFrame {

    public Display2D () 
    {
        initComponents();
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.Y_AXIS));
        
    }

    public Display2D (String name, int [ ] [ ] a) 
    {
        this ();
        put (name, a);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(200, 100));
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    public void put (String name, int [] [] a)
    {
        if (a == null)
        {
           setTitle (name + " == null");
        }
        else
        {
           int n = a.length;
           setTitle (name+ ", " + name + ".length == " + n);
        
        
            Container target = getContentPane();
            /*
            int x = 10;
            int y = 10;
            int w = this.getWidth() - 2*x;
            int h = 40;
            int delta = y;
            */

            for (int i = 0; i < n; i++)
            {
               DisplayPanel d = new DisplayPanel (name + "[" + i + "]", a[i]);
               target.add (d);
               /*
               target.add (d, new AbsoluteConstraints (x, y, w, h));
               y = y + h + delta;
               */
            }
            
            pack ();
        }

    }
    
    private static int x = 10;
    private static int y = 10;
    
    public static void demo (String name, int [] [] a )
    {
        Display2D w = new Display2D (name, a);
        w.setVisible (true);
        w.setLocation (x, y);
        x = x + 40;
        y = y + 40;
    }
    
    public static void main(String args[]) {
        int [] [] a = { { 1, 2, 3}, { 10, 20 } };
        int [] [] b = new int [5] [5];
        int [] [] c = new int [3] [ ];
        int [] [] d = new int [0] [0];
        int [] [] e = null;
        demo ("a", a);
        demo ("b", b);
        demo ("c", c);
        demo ("d", d);
        demo ("e", e);
        
        int [] [] t = new int [5] [ ];
        int base = 1;
        for (int i = 0; i < t.length; i++)
        {
            t[i] = new int [t.length - i];
            for (int k = 0; k < t[i].length; k++)
                t[i][k] = (k+1) * base;
            base *=  10;
        }
       
        demo ("t", t);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
