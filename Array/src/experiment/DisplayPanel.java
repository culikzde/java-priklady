package experiment;

import java.awt.*;
import javax.swing.table.*;

public class DisplayPanel extends javax.swing.JPanel {

    public DisplayPanel () 
    {
        initComponents();
    }

    public DisplayPanel (String name, int [] a) 
    {
        initComponents();
        put (name, a);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setBorder(javax.swing.BorderFactory.createEtchedBorder());
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("jLabel1");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 12, -1, -1));

        jTextField1.setText("jTextField1");
        add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 30, 70, 30));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.setAlignmentX(0.7F);
        jTable1.setRowHeight(32);
        jScrollPane1.setViewportView(jTable1);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, 760, 59));
    }// </editor-fold>//GEN-END:initComponents

    public void put (String name, int a [])
    {
        // setTitle (name);
        if (a == null)
        {
           jLabel1.setText ("null");
           jLabel1.setForeground (new Color (255, 0, 0));
           jLabel1.setFont(new java.awt.Font("Dialog", 0, 36)); 
           
           jTextField1.setText ("null");
           jTextField1.setForeground (new Color (255, 0, 0));
           // remove (jTable1);
           // getContentPane().remove (jS)
           jTextField1.setVisible (false);        
           jScrollPane1.setVisible (false);        
        }
        else
        {
            int n = a.length;

            jLabel1.setText (name + ".length");
            jTextField1.setText ("" + n);// convert to string

            String [] title = new String [n];
            String [] [] data = new String [1] [n];

            for (int i = 0; i < n; i++) 
            {
                title [i] = name + "[" + i + "]" ;
                data [0][i] = "" + a[i]; // convert to string

            }

            DefaultTableModel model = new DefaultTableModel (data, title);
            jTable1.setModel (model);
            
            TableColumnModel columnModel = jTable1.getColumnModel();
            for (int i = 0; i < n; i++) 
                columnModel.getColumn(i).setMaxWidth (80);
                
            if (n == 0)
               jScrollPane1.setVisible (false);        
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
